package de.n26.controller;

import de.n26.dto.Statistics;
import de.n26.dto.Transaction;
import de.n26.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StatisticsController {

    @Autowired
    private StatisticsService statisticsService;

    @PostMapping(value = "/statistics")
    public ResponseEntity postStatistics(@Validated @RequestBody Transaction transaction) {
        boolean isAdded = statisticsService.addEntryForLastMinute(transaction.getAmount(), transaction.getTimestamp());

        return isAdded ? ResponseEntity.status(HttpStatus.CREATED).body(null) : ResponseEntity.status(HttpStatus
                .NO_CONTENT).body(null);
    }

    @GetMapping(value = "/statistics")
    public @ResponseBody
    Statistics getStatistics() {
        return statisticsService.getForLastMinute().toDto();
    }
}