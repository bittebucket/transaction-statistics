package de.n26.model;

import de.n26.dto.Statistics;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class RollingStatistics {
    private double sum;
    private double avg;
    private double max;
    private double min;
    private long count;

    public RollingStatistics(final RollingStatistics oth) {
        sum = oth.sum;
        avg = oth.avg;
        max = oth.max;
        min = oth.min;
        count = oth.count;
    }

    public void addValue(double value) {
        sum = sum + value;
        count++;
        avg = sum / count;
        if (count == 1) {
            min = value;
            max = value;
        } else {
            min = Double.min(min, value);
            max = Double.max(max, value);
        }
    }

    public Statistics toDto() {
        Statistics statistics = new Statistics();
        statistics.setSum(sum);
        statistics.setAvg(avg);
        statistics.setMax(max);
        statistics.setMin(min);
        statistics.setCount(count);

        return statistics;
    }
}
