package de.n26.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Statistics {
    private double sum;
    private double avg;
    private double max;
    private double min;
    private long count;
}
