package de.n26.dto;

import lombok.Getter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
public class Transaction {
    @NotNull(message = "Amount of transaction is required")
    @Min(value = 0, message = "Negative transactions are not supported in statistics")
    private Double amount;

    @NotNull(message = "Timestamp of transaction is required")
    @Min(value = 0, message = "Invalid timestamp")
    private Long timestamp;
}
