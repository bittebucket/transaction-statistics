package de.n26.service;

import de.n26.exception.BadRequestException;
import de.n26.model.RollingStatistics;
import org.springframework.stereotype.Service;

import java.util.NavigableMap;
import java.util.Optional;
import java.util.TreeMap;

@Service
public class StatisticsService {

    private static final long DURATION_ONE_MINUTE = 60000;

    private NavigableMap<Long, RollingStatistics> timestampToRollingStatisticsMap = new TreeMap<>();

    public synchronized boolean addEntryForLastMinute(double amount, long timestamp) {
        long currentTimestamp = System.currentTimeMillis();
        long begin = currentTimestamp - DURATION_ONE_MINUTE;

        if (timestamp > currentTimestamp) {
            throw new BadRequestException("Future transactions are not supported");
        }

        if (timestamp > begin) {
            Long timestampToDelete = timestampToRollingStatisticsMap.floorKey(begin);
            if (timestampToDelete != null) {
                timestampToRollingStatisticsMap.headMap(timestampToDelete).clear();
            }

            timestampToRollingStatisticsMap.forEach((key, value) -> {
                if (key < timestamp) value.addValue(amount);
            });

            RollingStatistics rollingStatistics = new RollingStatistics(
                    Optional.ofNullable(timestampToRollingStatisticsMap.ceilingEntry(timestamp))
                            .map(e -> e.getValue()).orElse(new RollingStatistics()));
            rollingStatistics.addValue(amount);
            timestampToRollingStatisticsMap.put(timestamp, rollingStatistics);

            return true;
        } else {
            return false;
        }
    }

    public synchronized RollingStatistics getForLastMinute() {
        long begin = System.currentTimeMillis() - DURATION_ONE_MINUTE;

        return Optional.ofNullable(timestampToRollingStatisticsMap.ceilingKey(begin))
                .map(key -> timestampToRollingStatisticsMap.get(key))
                .orElse(new RollingStatistics());
    }


    protected void reset() {
        timestampToRollingStatisticsMap.clear();
    }
}