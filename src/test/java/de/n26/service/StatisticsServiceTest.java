package de.n26.service;

import de.n26.model.RollingStatistics;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {StatisticsService.class})
public class StatisticsServiceTest {

    @Autowired
    private StatisticsService statisticsService;

    @Before
    public void init() {
        statisticsService.reset();
    }

    @Test
    public void addEntryForLastMinuteExpired() throws Exception {
        long expiredTimestamp = System.currentTimeMillis() - 120000;
        assertFalse(statisticsService.addEntryForLastMinute(10, expiredTimestamp));
    }

    @Test
    public void addEntryForLastMinuteFresh() throws Exception {
        assertTrue(statisticsService.addEntryForLastMinute(10, System.currentTimeMillis()));
    }

    @Test
    public void getForLastMinuteForNoTransactions() throws Exception {
        RollingStatistics rollingStatistics = statisticsService.getForLastMinute();

        assertEquals(0, rollingStatistics.getSum(), 0.01);
        assertEquals(0, rollingStatistics.getAvg(), 0.01);
        assertEquals(0, rollingStatistics.getMax(), 0.01);
        assertEquals(0, rollingStatistics.getMin(), 0.01);
        assertEquals(0, rollingStatistics.getCount());
    }

    @Test
    public void getForLastMinuteForOneTransaction() throws Exception {
        statisticsService.addEntryForLastMinute(10,  System.currentTimeMillis());
        RollingStatistics rollingStatistics = statisticsService.getForLastMinute();

        assertEquals(10, rollingStatistics.getSum(), 0.01);
        assertEquals(10, rollingStatistics.getAvg(), 0.01);
        assertEquals(10, rollingStatistics.getMax(), 0.01);
        assertEquals(10, rollingStatistics.getMin(), 0.01);
        assertEquals(1, rollingStatistics.getCount());
    }

    @Test
    public void getForLastMinuteForOneTransactionAfterItExpires() throws Exception {
        statisticsService.addEntryForLastMinute(10,  System.currentTimeMillis() - 59000);
        Thread.sleep(2000);

        RollingStatistics rollingStatistics = statisticsService.getForLastMinute();

        assertEquals(0, rollingStatistics.getSum(), 0.01);
        assertEquals(0, rollingStatistics.getAvg(), 0.01);
        assertEquals(0, rollingStatistics.getMax(), 0.01);
        assertEquals(0, rollingStatistics.getMin(), 0.01);
        assertEquals(0, rollingStatistics.getCount());
    }

    @Test
    public void getForLastMinuteForMultipleTransactions() throws Exception {
        long timestamp = System.currentTimeMillis();
        statisticsService.addEntryForLastMinute(10,  timestamp - 1);
        statisticsService.addEntryForLastMinute(20,  timestamp - 2);
        statisticsService.addEntryForLastMinute(30,  timestamp);
        statisticsService.addEntryForLastMinute(40,  timestamp - 1);
        RollingStatistics rollingStatistics = statisticsService.getForLastMinute();

        assertEquals(100, rollingStatistics.getSum(), 0.01);
        assertEquals(25, rollingStatistics.getAvg(), 0.01);
        assertEquals(40, rollingStatistics.getMax(), 0.01);
        assertEquals(10, rollingStatistics.getMin(), 0.01);
        assertEquals(4, rollingStatistics.getCount());
    }

    @Test
    public void getForLastMinuteForMultipleTransactionsWithSameTimestamp() throws Exception {
        long timestamp = System.currentTimeMillis();
        statisticsService.addEntryForLastMinute(10,  timestamp);
        statisticsService.addEntryForLastMinute(20,  timestamp);
        RollingStatistics rollingStatistics = statisticsService.getForLastMinute();

        assertEquals(30, rollingStatistics.getSum(), 0.01);
        assertEquals(15, rollingStatistics.getAvg(), 0.01);
        assertEquals(20, rollingStatistics.getMax(), 0.01);
        assertEquals(10, rollingStatistics.getMin(), 0.01);
        assertEquals(2, rollingStatistics.getCount());
    }

    @Test
    public void getForLastMinuteForStatisticsMustChangeWithExpiredTransaction() throws Exception {
        long timestamp = System.currentTimeMillis();
        statisticsService.addEntryForLastMinute(10, timestamp);
        statisticsService.addEntryForLastMinute(20, timestamp - 59000);
        Thread.sleep(2000);

        RollingStatistics rollingStatistics = statisticsService.getForLastMinute();

        assertEquals(10, rollingStatistics.getSum(), 0.01);
        assertEquals(10, rollingStatistics.getAvg(), 0.01);
        assertEquals(10, rollingStatistics.getMax(), 0.01);
        assertEquals(10, rollingStatistics.getMin(), 0.01);
        assertEquals(1, rollingStatistics.getCount());
    }
}