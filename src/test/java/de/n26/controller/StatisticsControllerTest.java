package de.n26.controller;

import de.n26.service.StatisticsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = {StatisticsController.class, StatisticsService.class})
@AutoConfigureMockMvc
@EnableWebMvc
public class StatisticsControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private StatisticsService statisticsService;

    @Test
    public void postStatisticsValidTransaction() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/statistics").contentType(MediaType.APPLICATION_JSON)
                .content("{\"amount\": 12.3, \"timestamp\": " + System.currentTimeMillis() + "}"))
                .andExpect(status().isCreated())
                .andExpect(content().string(""));
    }

    @Test
    public void postStatisticsOldTransaction() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/statistics").contentType(MediaType.APPLICATION_JSON)
                .content("{\"amount\": 12.3, \"timestamp\": " + (System.currentTimeMillis() - 120000) + "}"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void postStatisticsMissingAmount() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/statistics").contentType(MediaType.APPLICATION_JSON)
                .content("{\"timestamp\": " + System.currentTimeMillis() + "}"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(""));
    }

    @Test
    public void postStatisticsMissingTimestamp() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/statistics").contentType(MediaType.APPLICATION_JSON)
                .content("{\"amount\": 12.3}"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(""));
    }

    @Test
    public void postStatisticsNegativeAmount() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/statistics").contentType(MediaType.APPLICATION_JSON)
                .content("{\"amount\": -10, \"timestamp\": " + (System.currentTimeMillis()) + "}"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(""));
    }

    @Test
    public void postStatisticsInvalidTimestamp() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/statistics").contentType(MediaType.APPLICATION_JSON)
                .content("{\"amount\": 12.3, \"timestamp\": " + (System.currentTimeMillis() + 120000) + "}"))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(""));
    }

    @Test
    public void getStatistics() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/statistics"))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"sum\":0.0,\"avg\":0.0,\"max\":0.0,\"min\":0.0,\"count\":0}"));
    }
}