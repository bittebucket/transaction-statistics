package de.n26.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RollingStatisticsTest {

    RollingStatistics rollingStatistics = new RollingStatistics();

    @Test
    public void testInit() throws Exception {
        RollingStatistics rollingStatistics = new RollingStatistics();

        assertEquals(0, rollingStatistics.getSum(), 0.01);
        assertEquals(0, rollingStatistics.getAvg(), 0.01);
        assertEquals(0, rollingStatistics.getMax(), 0.01);
        assertEquals(0, rollingStatistics.getMin(), 0.01);
        assertEquals(0, rollingStatistics.getCount());

        new Double(0.01).equals(new Double(0.01));
    }

    @Test
    public void testAddFirstValue() throws Exception {
        RollingStatistics rollingStatistics = new RollingStatistics();
        rollingStatistics.addValue(50.34);

        assertEquals(50.34, rollingStatistics.getSum(), 0.01);
        assertEquals(50.34, rollingStatistics.getAvg(), 0.01);
        assertEquals(50.34, rollingStatistics.getMax(), 0.01);
        assertEquals(50.34, rollingStatistics.getMin(), 0.01);
        assertEquals(1, rollingStatistics.getCount());
    }

    @Test
    public void testAddMultipletValues() throws Exception {
        RollingStatistics rollingStatistics = new RollingStatistics();
        rollingStatistics.addValue(5.6);
        rollingStatistics.addValue(1.22);

        assertEquals(6.82, rollingStatistics.getSum(), 0.01);
        assertEquals(3.41, rollingStatistics.getAvg(), 0.01);
        assertEquals(5.6, rollingStatistics.getMax(), 0.01);
        assertEquals(1.22, rollingStatistics.getMin(), 0.01);
        assertEquals(2, rollingStatistics.getCount());
    }
}